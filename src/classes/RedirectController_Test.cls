@isTest
private class RedirectController_Test {

	static testMethod void noObjName() {
        User u = buildUser();
       
        system.runAs(u) {
        	 Account acc = buildAccount();
        	 
        	 Test.StartTest();
	        
	         PageReference redirectPageRef = Page.Redirect;
	         Test.setCurrentPage(redirectPageRef);
	         //Set test params
	         
	         RedirectController controller = new RedirectController();
	         PageReference redirectedPage = controller.redirect();
	         system.assert(controller.errorMessage.startsWithIgnoreCase('No object name provided.'));
	         system.assertEquals(null, redirectedPage);
	         
	         Test.StopTest();
        }
    }
	
	static testMethod void badObjName() {
        User u = buildUser();
       
        system.runAs(u) {
        	 Account acc = buildAccount();
        	 
        	 Test.StartTest();
	        
	         PageReference redirectPageRef = Page.Redirect;
	         Test.setCurrentPage(redirectPageRef);
	         //Set test params
			 ApexPages.currentPage().getParameters().put('obj','Acc');
			 ApexPages.currentPage().getParameters().put('name',acc.Name);
	         
	         RedirectController controller = new RedirectController();
	         PageReference redirectedPage = controller.redirect();
	         system.assert(controller.errorMessage.startsWithIgnoreCase('Could not find a Name field'));
	         system.assertEquals(null, redirectedPage);
	         
	         Test.StopTest();
        }
    }
    
    static testMethod void noName() {
        User u = buildUser();
       
        system.runAs(u) {
        	 Account acc = buildAccount();
        	 
        	 Test.StartTest();
	        
	         PageReference redirectPageRef = Page.Redirect;
	         Test.setCurrentPage(redirectPageRef);
	         //Set test params
			 ApexPages.currentPage().getParameters().put('obj','Account');
	         
	         RedirectController controller = new RedirectController();
	         PageReference redirectedPage = controller.redirect();
	         system.assert(controller.errorMessage.startsWithIgnoreCase('No record name provided.'));
	         system.assertEquals(null, redirectedPage);
	         
	         Test.StopTest();
        }
    }
    
    static testMethod void badName() {
        User u = buildUser();
       
        system.runAs(u) {
        	 Account acc = buildAccount();
        	 
        	 Test.StartTest();
	        
	         PageReference redirectPageRef = Page.Redirect;
	         Test.setCurrentPage(redirectPageRef);
	         //Set test params
			 ApexPages.currentPage().getParameters().put('obj','Account');
			 ApexPages.currentPage().getParameters().put('name',acc.ID);
	         
	         RedirectController controller = new RedirectController();
	         PageReference redirectedPage = controller.redirect();
	         system.assert(controller.errorMessage.startsWithIgnoreCase('Could not find a(n)'));
	         system.assertEquals(null, redirectedPage);
	         
	         Test.StopTest();
        }
    }
    
    static testMethod void redirectAccountSuccessfully() {
        User u = buildUser();
       
        system.runAs(u) {
        	 Account acc = buildAccount();
        	 
        	 Test.StartTest();
	        
	         PageReference redirectPageRef = Page.Redirect;
	         Test.setCurrentPage(redirectPageRef);
	         //Set test params
			 ApexPages.currentPage().getParameters().put('obj','Account');
			 ApexPages.currentPage().getParameters().put('name',acc.Name);
	         
	         RedirectController controller = new RedirectController();
	         PageReference redirectedPage = controller.redirect();
	         system.assertEquals(null, controller.errorMessage);
	         system.assertNotEquals(null, redirectedPage);
	         system.assert(redirectedPage.getUrl().startsWithIgnoreCase('/'+acc.Id));
	         
	         Test.StopTest();
        }
    }
    
    static testMethod void redirectCaseSuccessfully() {
        User u = buildUser();
       
        system.runAs(u) {
        	 Case c = buildCase();
        	 
        	 Test.StartTest();
	        
	         PageReference redirectPageRef = Page.Redirect;
	         Test.setCurrentPage(redirectPageRef);
	         //Set test params
			 ApexPages.currentPage().getParameters().put('obj','Case');
			 ApexPages.currentPage().getParameters().put('name',c.CaseNumber);
	         
	         RedirectController controller = new RedirectController();
	         PageReference redirectedPage = controller.redirect();
	         system.assertEquals(null, controller.errorMessage);
	         system.assertNotEquals(null, redirectedPage);
	         system.assert(redirectedPage.getUrl().startsWithIgnoreCase('/'+c.Id));
	         
	         Test.StopTest();
        }
    }
    
    public static User buildUser() {
        Profile p = [select id from Profile where name = 'System Administrator' LIMIT 1];
        
        User u = new User(alias = 'abc', 
			            lastname='mahood', 
			            email='andy@test.com',
			            emailencodingkey='UTF-8', 
			            languagelocalekey='en_US',
			            localesidkey='en_US', 
			            timezonesidkey='America/Los_Angeles', 
			            username='andy@test.com',
			            profileid=p.id);
          
        return u;
    }
    
    public static Account buildAccount() {
    	Account acc = new Account(Name='Tapply');
    	insert acc;
    	
    	return acc;
    }
    
    public static Case buildCase() {
    	Case c = new Case(Subject='Help!');
    	insert c;
    	
    	c = [SELECT CaseNumber FROM Case WHERE Id = :c.ID LIMIT 1];
    	
    	return c;
    }
}