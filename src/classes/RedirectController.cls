public with sharing class RedirectController {
	
	public string errorMessage{get;set;}
	
	public RedirectController() {
		
	}
	
	public PageReference redirect() {
		//retrive the required params from the URL
		string objName=ApexPages.currentPage().getParameters().get('obj');
		if (objName==null) {
			errorMessage='No object name provided. Add a URL parameter named obj with the API Name of the Object';
			return null; 
		}
		string name=ApexPages.currentPage().getParameters().get('name');
		if (name==null) {
			errorMessage='No record name provided. Add a URL parameter named name with the Name of the record';
			return null; 
		}
		//do some describing so we get the Objects Name Field Name
		string fieldName = getObjFieldName( objName );
		if (fieldName==null) {
			errorMessage='Could not find a Name field to search on for the object ' + objName;
			return null; 
		}
		
		//get all parems in a map as we need to chukc them on to the new page ref
		Map<String, String> params = ApexPages.currentPage().getParameters();
		
		sObject record;
		//dynamic query using the object name and the field foind to be the name field
		//use limit one a list as the there may be more than one or none returned
		List<sObject> records = database.query('SELECT ID FROM '+objName+' WHERE '+fieldName+' = :name LIMIT 1');
		if (records!=null && !records.isEmpty()) {
			record = records[0];//select the one of the top
		} else {
			errorMessage='Could not find a(n) '+objName+' called ' + name;
			return null; 
		}
		
		//build first part of URL...
		string redirectURL = '/' + record.Id + '?';
		PageReference pageRef = new PageReference(redirectURL);
		
		//loop over params passing them onto the new page reference
		for (String paramKey:params.keyset()) {
			String paramValue = params.get( paramKey );
			pageRef.getParameters().put(paramKey, paramValue);
		}

		return pageRef;
	}
	
	private String getObjFieldName( string objName ) {	
		/***
		this method attempts to find the Name field from the object requests
		Name fields are not always called name e.g. CaseNumber, SolutionNumber etc.
		***/
		Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe(); 
		Schema.SObjectType sObjectType = globalDescribe.get( objName );
		if (sObjectType==null) {
			return null;
		}
		
		Map<String, Schema.SObjectField> fieldMap = sObjectType.getDescribe().fields.getMap();
		for (Schema.SObjectField sObjField : fieldMap.values()) {
			Schema.DescribeFieldResult sObjFieldDesc = sObjField.getDescribe();

			if (sObjFieldDesc.isNameField()) { 
				//This where i add to the map
				return sObjFieldDesc.getName();
			}
		}
		
		return null;
	}
}